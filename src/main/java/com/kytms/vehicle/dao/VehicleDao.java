package com.kytms.vehicle.dao;

import com.kytms.core.dao.BaseDao;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 车型DAO
 *
 * @author 陈小龙
 * @create 2018-01-10
 */
public interface VehicleDao<Vehicle>  extends BaseDao<Vehicle> {
}
