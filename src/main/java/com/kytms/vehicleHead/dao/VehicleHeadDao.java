package com.kytms.vehicleHead.dao;

import com.kytms.core.dao.BaseDao;

import java.util.List;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 车头档案DAO层
 *
 * @author 陈小龙
 * @create 2018-01-12
 */
public interface VehicleHeadDao<VehicelHead> extends BaseDao<VehicelHead> {

}
