package com.kytms.carrier.dao.impl;

import com.kytms.carrier.dao.CarrierDao;
import com.kytms.carrier.service.impl.CarrierServiceImpl;
import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Carrier;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * 承运商设置DAO层实现类
 * @author 陈小龙
 * @create 2018-01-15
 */
@Repository(value = "CarrierDao")
public class CarrierDaoImpl extends BaseDaoImpl<Carrier> implements CarrierDao<Carrier> {
    private Logger log = Logger.getLogger(CarrierDaoImpl.class);//输出Log日志
}
